package com.tant.rest.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public class DateTimeUtils {


    public static LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static ZonedDateTime toZonedDateTime(final LocalDateTime dateTime) {
        return dateTime == null ? null : ZonedDateTime.of(dateTime, ZoneId.systemDefault());
    }

    public static LocalDateTime toLocalDateTime(final ZonedDateTime dateTime) {
        return dateTime == null ? null : dateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }

    private DateTimeUtils() {
        super();
    }
}
