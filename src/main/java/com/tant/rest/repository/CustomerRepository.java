package com.tant.rest.repository;

import com.tant.rest.model.entity.CustomerEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
public interface CustomerRepository extends JpaRepository<CustomerEntity, UUID>,
    JpaSpecificationExecutor<CustomerEntity> {

}
