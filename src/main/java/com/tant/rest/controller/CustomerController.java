package com.tant.rest.controller;


import com.tant.rest.Const;
import com.tant.rest.config.ApiPageable;
import com.tant.rest.model.dto.CustomerDto;
import com.tant.rest.model.dto.CustomerRequestDto;
import com.tant.rest.service.CustomerService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.UUID;

import static org.springframework.data.domain.Sort.Direction.ASC;

import lombok.RequiredArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@RestController
@RequestMapping(value = Const.API1
        + "customers", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiPageable
    public Page<CustomerDto> getCustomers(
            @ApiIgnore
            @PageableDefault(page = 0, size = 20, sort = "createdAt", direction = ASC) final Pageable pageFilter
    ) {
        return customerService.getCustomers(pageFilter);
    }

    @GetMapping("/{customerId}")
    @ResponseStatus(HttpStatus.OK)
    public CustomerDto getCustomer(@PathVariable("customerId") final UUID customerId) {
        return customerService.getCustomer(customerId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createCustomer(@Valid @RequestBody final CustomerRequestDto request) {
        customerService.createCustomer(request);
    }

    @DeleteMapping("/{customerId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCustomer(@PathVariable("customerId") final UUID customerId) {
        customerService.deleteCustomer(customerId);
    }

    @PutMapping("/{customerId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateCustomer(@PathVariable("customerId") final UUID customerId,
                              @Valid @RequestBody final CustomerRequestDto request) {
        customerService.updateCustomer(customerId, request);
    }
}
