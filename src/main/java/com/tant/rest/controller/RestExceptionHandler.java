package com.tant.rest.controller;

import com.tant.rest.exception.AuthenticateException;
import com.tant.rest.exception.ValidationException;
import com.tant.rest.model.dto.ErrorDto;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.UnexpectedTypeException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {


    @ExceptionHandler(AuthenticateException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorDto handleException(final AuthenticateException ex) {
        return new ErrorDto(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleException(final MethodArgumentNotValidException ex) {
        return new ErrorDto("invalid.argument", ex.getBindingResult().getAllErrors().get(0).getDefaultMessage());
    }


    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleException(final ValidationException ex) {
        log.error(ex.getMessage(), ex);

        return new ErrorDto(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(UnexpectedTypeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleException(final UnexpectedTypeException ex) {
        return new ErrorDto("invalid.argument", ex.getMessage());
    }

}
