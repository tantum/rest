package com.tant.rest.controller;


import com.tant.rest.Const;
import com.tant.rest.config.ApiPageable;
import com.tant.rest.model.dto.ProductDto;
import com.tant.rest.model.dto.ProductRequestDto;
import com.tant.rest.service.ProductService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.UUID;

import static org.springframework.data.domain.Sort.Direction.ASC;

import lombok.RequiredArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@RestController
@RequestMapping(value = Const.API1,
    consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/customers/{customerId}/products")
    @ResponseStatus(HttpStatus.OK)
    @ApiPageable
    public Page<ProductDto> getProducts(@PathVariable("customerId") final UUID customerId,
            @ApiIgnore
            @PageableDefault(page = 0, size = 20, sort = "createdAt", direction = ASC) final Pageable pageFilter
    ) {
        return productService.getProducts(pageFilter, customerId);
    }

    @GetMapping("/products/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto getProduct(@PathVariable("productId") final UUID productId) {
        return productService.getProduct(productId);
    }

    @PostMapping("/customers/{customerId}/products")
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@PathVariable("customerId") final UUID customerId,
            @Valid @RequestBody final ProductRequestDto request) {
        productService.createProduct(request, customerId);
    }

    @DeleteMapping("/products/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProduct(@PathVariable("productId") final UUID productId) {
        productService.deleteProduct(productId);
    }

    @PutMapping("/products/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateProduct(@PathVariable("productId") final UUID productId,
                              @Valid @RequestBody final ProductRequestDto request) {
        productService.updateProduct(productId, request);
    }
}
