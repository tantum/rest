package com.tant.rest.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@AllArgsConstructor
@Getter
@ToString
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = -3079775331433990015L;

    private final String code;
    private final String message;

}
