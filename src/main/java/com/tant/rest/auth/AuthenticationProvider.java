package com.tant.rest.auth;

import com.tant.rest.exception.AuthenticateException;
import com.tant.rest.service.AuthenticateService;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
@Component
public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private final AuthenticateService authenticateService;

    public AuthenticationProvider(final AuthenticateService authenticateService) {
        super();
        this.authenticateService = authenticateService;
    }

    @Override
    protected void additionalAuthenticationChecks(final UserDetails userDetails,
            final UsernamePasswordAuthenticationToken authentication)
            throws org.springframework.security.core.AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(final String username, final UsernamePasswordAuthenticationToken authentication)
            throws org.springframework.security.core.AuthenticationException {
        final Object token = authentication.getCredentials();
        return Optional.ofNullable(token).map(String::valueOf).flatMap(authenticateService::findByToken).orElseThrow(
                () -> new AuthenticateException("invalid token", "cannot find user with authentication token=" + token));
    }

}