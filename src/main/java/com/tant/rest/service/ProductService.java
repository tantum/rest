package com.tant.rest.service;

import com.tant.rest.model.dto.ProductDto;
import com.tant.rest.model.dto.ProductRequestDto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
public interface ProductService {

    void createProduct(ProductRequestDto request, UUID customerId);

    Page<ProductDto> getProducts(Pageable pageFilter, UUID customerId);

    void updateProduct(UUID id, ProductRequestDto request);

    void deleteProduct(UUID id);

    ProductDto getProduct(UUID id);
}
