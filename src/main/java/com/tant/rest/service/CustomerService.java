package com.tant.rest.service;

import com.tant.rest.model.dto.CustomerDto;
import com.tant.rest.model.dto.CustomerRequestDto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
public interface CustomerService {

    void createCustomer(CustomerRequestDto request);

    void updateCustomer(UUID id, CustomerRequestDto request);

    void deleteCustomer(UUID id);

    CustomerDto getCustomer(UUID id);

    Page<CustomerDto> getCustomers(Pageable pageFilter);

}
