package com.tant.rest.service.impl;

import com.tant.rest.service.AuthenticateService;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
@Service
public class AuthenticateServiceImpl implements AuthenticateService {

    @Override
    public Optional<UserDetails> findByToken(final String token) {
        return "token".equalsIgnoreCase(token) ? Optional.ofNullable(mockUserDetails()) : Optional.empty();
    }

    private UserDetails mockUserDetails() {
        return new UserDetails() {

            private static final long serialVersionUID = 1L;

            @Override
            public boolean isEnabled() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public String getUsername() {
                return "mock";
            }

            @Override
            public String getPassword() {
                return "mock_pwd";
            }

            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Collections.emptyList();
            }
        };
    }

}
