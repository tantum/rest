package com.tant.rest.service.impl;

import com.tant.rest.exception.EntityNotFoundException;
import com.tant.rest.exception.ValidationException;
import com.tant.rest.mapper.ProductMapper;
import com.tant.rest.model.dto.CustomerDto;
import com.tant.rest.model.dto.ProductDto;
import com.tant.rest.model.dto.ProductRequestDto;
import com.tant.rest.model.entity.CustomerEntity;
import com.tant.rest.model.entity.ProductEntity;
import com.tant.rest.repository.ProductRepository;
import com.tant.rest.service.CustomerService;
import com.tant.rest.service.ProductService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import lombok.RequiredArgsConstructor;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CustomerService customerService;

    @Transactional
    @Override
    public void createProduct(final ProductRequestDto request, final UUID customerId) {
        final ProductEntity entity = buildEntity(request, customerId);
        productRepository.save(entity);
    }


    @Override
    public Page<ProductDto> getProducts(final Pageable pageFilter, final UUID customerId) {
        final CustomerDto customer = customerService.getCustomer(customerId);
        return productRepository.findAll(createNoDeleteSpecification(customerId), pageFilter)
                .map(tmp -> ProductMapper.toDto(tmp, customer));
    }

    @Transactional
    @Override
    public void updateProduct(final UUID id, final ProductRequestDto request) {
        final ProductEntity entity = getProductEntity(id);
        checkActiveProduct(entity);
        fillData(request, entity);
        productRepository.save(entity);
    }

    @Transactional
    @Override
    public void deleteProduct(final UUID id) {
        final ProductEntity entity = getProductEntity(id);
        checkActiveProduct(entity);

        entity.setDeleted(true);
        productRepository.save(entity);
    }

    @Override
    public ProductDto getProduct(final UUID id) {
        final ProductEntity entity = getProductEntity(id);
        checkActiveProduct(entity);
        return ProductMapper.toDto(entity, customerService.getCustomer(entity.getCustomerId()));
    }

    private ProductEntity buildEntity(final ProductRequestDto request, final UUID customerId) {
        final ProductEntity entity = new ProductEntity();
        fillData(request, entity);
        final CustomerEntity customer = new CustomerEntity();
        entity.setCustomerId(customerId);
        return entity;
    }

    private void checkActiveProduct(final ProductEntity entity) {
        if (entity.isDeleted()) {
            throw new ValidationException("product.already.deleted", "product has already been deleted");
        }
    }

    private ProductEntity getProductEntity(final UUID id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("product.not.found", "product not found"));
    }

    private void fillData(final ProductRequestDto request, final ProductEntity entity) {
        entity.setTitle(request.getTitle());
        entity.setDescription(request.getDescription());
        entity.setPrice(request.getPrice());
    }

    public Specification<ProductEntity> createNoDeleteSpecification(final UUID customerId) {
        return (root, query, cb) -> {
            // can add hibernate-jpamodelgen for using Customer_.isDeleted
            cb.and(cb.equal(root.get("customerId"), customerId));
            return cb.and(cb.equal(root.get("isDeleted"), false));
        };
    }
}
