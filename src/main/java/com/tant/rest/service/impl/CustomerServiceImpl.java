package com.tant.rest.service.impl;

import com.tant.rest.exception.EntityNotFoundException;
import com.tant.rest.exception.ValidationException;
import com.tant.rest.mapper.CustomerMapper;
import com.tant.rest.model.dto.CustomerDto;
import com.tant.rest.model.dto.CustomerRequestDto;
import com.tant.rest.model.entity.CustomerEntity;
import com.tant.rest.repository.CustomerRepository;
import com.tant.rest.service.CustomerService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import lombok.RequiredArgsConstructor;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Transactional
    @Override
    public void createCustomer(final CustomerRequestDto request) {
        final CustomerEntity entity = buildEntity(request);
        customerRepository.save(entity);
    }

    @Transactional
    @Override
    public void updateCustomer(final UUID id, final CustomerRequestDto request) {
        final CustomerEntity entity = getCustomerEntity(id);
        checkActiveCustomer(entity);
        fillData(request, entity);
        customerRepository.save(entity);
    }

    @Transactional
    @Override
    public void deleteCustomer(final UUID id) {
        final CustomerEntity entity = getCustomerEntity(id);
        checkActiveCustomer(entity);

        entity.setDeleted(true);
        Optional.ofNullable(entity.getProducts()).ifPresent(tmp -> tmp.forEach(p -> p.setDeleted(true)));
        customerRepository.save(entity);
    }

    @Override
    public CustomerDto getCustomer(final UUID id) {
        final CustomerEntity entity = getCustomerEntity(id);
        checkActiveCustomer(entity); // TODO can be changed according business requirements
        return CustomerMapper.toDto(entity);
    }

    @Override
    public Page<CustomerDto> getCustomers(final Pageable pageFilter) {
        return customerRepository.findAll(createNoDeleteSpecification(), pageFilter).map(CustomerMapper::toDto);
    }

    private CustomerEntity buildEntity(final CustomerRequestDto request) {
        final CustomerEntity entity = new CustomerEntity();
        fillData(request, entity);
        return entity;
    }

    private void checkActiveCustomer(final CustomerEntity entity) {
        if (entity.isDeleted()) {
            throw new ValidationException("customer.already.deleted", "customer has already been deleted");
        }
    }

    private CustomerEntity getCustomerEntity(final UUID id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("customer.not.found", "customer not found"));
    }

    private void fillData(final CustomerRequestDto request, final CustomerEntity entity) {
        entity.setTitle(request.getTitle());
    }

    public Specification<CustomerEntity> createNoDeleteSpecification() {
        return (root, query, cb) -> {
            // can add hibernate-jpamodelgen for using Product_.isDeleted
            return cb.equal(root.get("isDeleted"), false);
        };
    }
}
