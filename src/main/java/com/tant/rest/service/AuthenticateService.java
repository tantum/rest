package com.tant.rest.service;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public interface AuthenticateService {

    Optional<UserDetails> findByToken(String token);
}
