package com.tant.rest;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public final class Const {

    public static final String API1 = "/api/v1/";
    public static final String BEARER = "Bearer ";

    private Const() {
        super();
    }
}
