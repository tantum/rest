package com.tant.rest.mapper;

import com.tant.rest.model.dto.CustomerDto;
import com.tant.rest.model.entity.CustomerEntity;
import com.tant.rest.utils.DateTimeUtils;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public class CustomerMapper {

    public static CustomerDto toDto(final CustomerEntity entity) {
        return CustomerDto.builder()
                .createdAt(DateTimeUtils.toZonedDateTime(entity.getCreatedAt()))
                .id(entity.getId())
                .isDeleted(entity.isDeleted())
                .title(entity.getTitle())
                .build();
    }

    private CustomerMapper() {
        super();
    }
}
