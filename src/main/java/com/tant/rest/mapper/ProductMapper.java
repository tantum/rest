package com.tant.rest.mapper;

import com.tant.rest.model.dto.CustomerDto;
import com.tant.rest.model.dto.ProductDto;
import com.tant.rest.model.entity.ProductEntity;
import com.tant.rest.utils.DateTimeUtils;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public class ProductMapper {

    public static ProductDto toDto(final ProductEntity entity, final CustomerDto customerDto) {
        return ProductDto.builder()
                .createdAt(DateTimeUtils.toZonedDateTime(entity.getCreatedAt()))
                .id(entity.getId())
                .isDeleted(entity.isDeleted())
                .title(entity.getTitle())
                .description(entity.getDescription())
                .price(entity.getPrice())
                .customer(customerDto)
                .build();
    }

    private ProductMapper() {
        super();
    }
}
