package com.tant.rest.model.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;

import static java.time.LocalDateTime.now;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@Entity
@Table(schema = "rest_db", name = "customer")
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class CustomerEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", unique = true, nullable = false)
    private UUID id;
    @Column(nullable = false)
    private String title;
    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerId", fetch = FetchType.LAZY)
    private List<ProductEntity> products;


    @PrePersist
    public void prePersist() {
        setCreatedAt(now(ZoneId.of("UTC")));
    }

    @PreUpdate
    public void preUpdate() {
        setModifiedAt(now(ZoneId.of("UTC")));
    }
}
