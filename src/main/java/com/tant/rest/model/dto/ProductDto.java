package com.tant.rest.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

import lombok.Builder;
import lombok.Value;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(builder = ProductDto.Builder.class)
@Builder(builderClassName = "Builder", toBuilder = true)
public class ProductDto {

    UUID id;
    String title;
    String description;
    BigDecimal price;
    boolean isDeleted;
    ZonedDateTime createdAt;
    CustomerDto customer;


    @JsonPOJOBuilder(withPrefix = "")
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class Builder {
    }
}
