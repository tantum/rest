package com.tant.rest.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRequestDto {

    @NotEmpty(message = "title cannot be empty")
    String title;
}
