package com.tant.rest.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductRequestDto {

    @NotEmpty(message = "title cannot be empty")
    String title;
    String description;
    @NotNull(message = "price cannot be empty")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    BigDecimal price;
}
