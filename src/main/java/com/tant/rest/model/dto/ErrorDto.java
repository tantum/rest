package com.tant.rest.model.dto;

import lombok.*;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-10
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ErrorDto {

    private String code;
    private String message;

}
