CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS customer (
    id                  UUID NOT NULL PRIMARY KEY,
    title               VARCHAR(255) NOT NULL,
    is_deleted          BOOLEAN NOT NULL DEFAULT FALSE,
    created_at          TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at         TIMESTAMP WITHOUT TIME ZONE
);


CREATE TABLE IF NOT EXISTS product (
    id                  UUID NOT NULL PRIMARY KEY,
    customer_id         UUID NOT NULL references customer (id),
    title               VARCHAR(255) NOT NULL,
    description         VARCHAR(1024),
    price               DECIMAL(10,2) NOT NULL,
    is_deleted          BOOLEAN NOT NULL DEFAULT FALSE,
    created_at          TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_at         TIMESTAMP WITHOUT TIME ZONE
);
