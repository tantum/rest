-- init script (must be executed on the environment before the migration)

-- create database
CREATE DATABASE rest_db;

-- NOW GO TO THE rest DB and execute the following:
-- 1. create new schema
CREATE SCHEMA IF NOT EXISTS rest_db;

-- 2. create new user
CREATE USER rest_user WITH PASSWORD 'rest_user_password';

-- 3. grant access for the new schema
GRANT ALL ON SCHEMA rest_db TO rest_user;
GRANT ALL ON ALL TABLES IN SCHEMA rest_db TO rest_user;
GRANT ALL ON ALL SEQUENCES IN SCHEMA rest_db TO rest_user;
