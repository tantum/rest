package com.tant.rest.config;

import com.zaxxer.hikari.HikariDataSource;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;

import java.net.URL;
import java.sql.Connection;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.tant.rest.repository")
@EntityScan(basePackages = "com.tant.rest.model.entity")
@Configuration
public class TestPostgresConfig {

    public static void initScript(final DataSource dataSource, final String sql) {
        try {
            final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            final URL dbScript = contextClassLoader.getResource(sql);
            try (final Connection conn = dataSource.getConnection()) {
                if (dbScript != null) {
                    conn.createStatement().execute(IOUtils.toString(dbScript.openStream()));
                }
            }
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Bean(destroyMethod = "close")
    public JdbcDatabaseContainer postgreSQLContainer() {
        final var container = new PostgreSQLContainer<>("postgres:9.6.16");
        container.start();
        return container;
    }

    @Primary
    @FlywayDataSource
    @ConfigurationProperties(prefix = "postgresql.hikari")
//    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean(destroyMethod = "close")
    public HikariDataSource dataSource(final JdbcDatabaseContainer postgreSQLContainer) {
        final var ds = new HikariDataSource();
        ds.setJdbcUrl(postgreSQLContainer.getJdbcUrl());
        ds.setUsername(postgreSQLContainer.getUsername());
        ds.setPassword(postgreSQLContainer.getPassword());

        //initScript(ds, "db/insert_users.sql");
        return ds;
    }
}
