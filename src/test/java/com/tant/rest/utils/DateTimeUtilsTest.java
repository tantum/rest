package com.tant.rest.utils;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public class DateTimeUtilsTest {

    @BeforeClass
    public static void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void test_getCurrentDateTime() {
        Assert.assertEquals(
                DateTimeUtils.getCurrentDateTime().toEpochSecond(ZoneOffset.UTC),
                LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
        );
    }


    @Test
    public void test_toLocalDateTime() {
        final LocalDateTime localDateTime = LocalDateTime.of(2020, 9, 28, 3, 1); // 1601251260000
        final ZonedDateTime zdtAtAsia = localDateTime.atZone(ZoneId.of("Europe/Moscow"));
        Assert.assertEquals(
                DateTimeUtils.toLocalDateTime(zdtAtAsia).toEpochSecond(ZoneOffset.UTC),
                1601251260L
        );
    }

    @Test
    public void test_toZonedDateTime() {
        final LocalDateTime localDateTime = LocalDateTime.now();
        Assert.assertEquals(
                TimeUnit.SECONDS.toMillis(DateTimeUtils.toZonedDateTime(localDateTime).toEpochSecond()),
                TimeUnit.SECONDS.toMillis(localDateTime.toEpochSecond(ZoneOffset.UTC))
        );
    }
}
