package com.tant.rest;

import com.jayway.restassured.RestAssured;
import com.tant.rest.config.TestPostgresConfig;
import com.tant.rest.config.WebSecurityConfig;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
@RunWith(SpringRunner.class)
@Sql({"classpath:db/insert_customers.sql"})
@Sql(value = "classpath:db/truncate.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {
        Application.class,
        TestPostgresConfig.class,
        WebSecurityConfig.class
})
@TestPropertySource(locations = "classpath:application-test.yml")
@EnableWebMvc
public class AbstractTestBase {

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = this.port;
    }

    @Test
    public void stub() {
        Assert.assertEquals(1, 1);
    }
}
