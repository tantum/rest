package com.tant.rest.controller;

import com.tant.rest.AbstractTestBase;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public class ProductControllerTest extends AbstractTestBase {

    private static final String TITLE = "title_" + UUID.randomUUID();
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).alwaysDo(print())
                .addFilter(springSecurityFilterChain)
                .build();
    }

    @Test
    public void testProductController_withoutBody() throws Exception {
        mockMvc.perform(createPostReqAuth("00000000-0000-0000-0000-000000000001")).andExpect(status().isBadRequest());
    }

    @Test
    public void testProductController_ok() throws Exception {
        mockMvc.perform(createPostReqAuth("00000000-0000-0000-0000-000000000001")
                .content("{\"title\": \"" + TITLE + "\",\"price\": \"" + 10000000.5 + "\"}"))
                .andExpect(status().isCreated());
    }

    @Test
    public void testProductController_getById() throws Exception {
        mockMvc.perform(addAuthHeader(get("/api/v1/products/50000000-0000-0000-0000-000000000001")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
              .andExpect(jsonPath("$.title", is("product1")))
              .andExpect(jsonPath("$.price", is(10000000.1)))
              .andExpect(jsonPath("$.id", is("50000000-0000-0000-0000-000000000001")))
              .andExpect(jsonPath("$.createdAt", notNullValue()))
              .andExpect(jsonPath("$.description", nullValue()))
              .andExpect(status().isOk());
    }

    @Test
    public void testCustomerController_getById_deleted() throws Exception {
        mockMvc.perform(addAuthHeader(get("/api/v1/products/50000000-0000-0000-0000-000000000002")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
              .andExpect(jsonPath("$.code", is("product.already.deleted")))
              .andExpect(status().isBadRequest());
    }

    @Test
    public void testProductController_delete() throws Exception {
        mockMvc.perform(addAuthHeader(delete("/api/v1/products/50000000-0000-0000-0000-000000000003")
                .contentType(MediaType.APPLICATION_JSON_VALUE))).andExpect(status().isOk());

        mockMvc.perform(addAuthHeader(delete("/api/v1/products/50000000-0000-0000-0000-000000000003")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
                .andExpect(jsonPath("$.code", is("product.already.deleted")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testProductController_put() throws Exception {
        mockMvc.perform(addAuthHeader(put("/api/v1/products/50000000-0000-0000-0000-000000000004")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"title\": \"" + TITLE + "\",\"price\": " + 10000000.5 + "}"))
        ).andExpect(status().isOk());

        mockMvc.perform(addAuthHeader(get("/api/v1/products/50000000-0000-0000-0000-000000000004")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
                .andExpect(jsonPath("$.title", is(TITLE)))
                .andExpect(jsonPath("$.price", is(1.00000005E7)))
                .andExpect(status().isOk());
    }

    private MockHttpServletRequestBuilder createPostReqAuth(final String id) {
        return addAuthHeader(post("/api/v1/customers/" + id + "/products")
                .contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    private MockHttpServletRequestBuilder addAuthHeader(final MockHttpServletRequestBuilder builder) {
        return builder.header(HttpHeaders.AUTHORIZATION, "Bearer token");
    }
}