package com.tant.rest.controller;

import com.tant.rest.AbstractTestBase;
import com.tant.rest.exception.AuthenticateException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author tant <a href="mailto:tantmbox@gmail.com" target="_top">email</a>
 * @version 1.0
 * @date 2020-08-11
 */
public class CustomerControllerTest extends AbstractTestBase {

    private static final String TITLE = "title_" + UUID.randomUUID();
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).alwaysDo(print())
                .addFilter(springSecurityFilterChain)
                .build();
            }

    @Test
    public void testCustomerController_withoutBody() throws Exception {
        mockMvc.perform(createPostReqAuth()).andExpect(status().isBadRequest());
    }

    @Test
    public void testCustomerController_ok() throws Exception {
        mockMvc.perform(createPostReqAuth()
                .content("{\"title\": \"" + TITLE + "\"}"))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCustomerController_getById() throws Exception {
        mockMvc.perform(addAuthHeader(get("/api/v1/customers/00000000-0000-0000-0000-000000000001")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
              .andExpect(jsonPath("$.title", is("title1")))
              .andExpect(jsonPath("$.id", is("00000000-0000-0000-0000-000000000001")))
              .andExpect(jsonPath("$.createdAt", notNullValue()))
              .andExpect(status().isOk());
    }

    @Test
    public void testCustomerController_getById_deleted() throws Exception {
        mockMvc.perform(addAuthHeader(get("/api/v1/customers/00000000-0000-0000-0000-000000000002")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
              .andExpect(jsonPath("$.code", is("customer.already.deleted")))
              .andExpect(status().isBadRequest());
    }

    @Test
    public void testCustomerController_delete() throws Exception {
        mockMvc.perform(addAuthHeader(delete("/api/v1/products/50000000-0000-0000-0000-000000000003")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
                .andExpect(status().isOk());


        mockMvc.perform(addAuthHeader(delete("/api/v1/customers/00000000-0000-0000-0000-000000000003")
                .contentType(MediaType.APPLICATION_JSON_VALUE))).andExpect(status().isOk());

        mockMvc.perform(addAuthHeader(delete("/api/v1/customers/00000000-0000-0000-0000-000000000003")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
                .andExpect(jsonPath("$.code", is("customer.already.deleted")))
                .andExpect(status().isBadRequest());

        mockMvc.perform(addAuthHeader(delete("/api/v1/products/50000000-0000-0000-0000-000000000003")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
        .andExpect(jsonPath("$.code", is("product.already.deleted")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCustomerController_put() throws Exception {
        mockMvc.perform(addAuthHeader(put("/api/v1/customers/00000000-0000-0000-0000-000000000004")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"title\": \"" + TITLE + "\"}"))
        ).andExpect(status().isOk());

        mockMvc.perform(addAuthHeader(get("/api/v1/customers/00000000-0000-0000-0000-000000000004")
                .contentType(MediaType.APPLICATION_JSON_VALUE)))
                .andExpect(jsonPath("$.title", is(TITLE)))
                .andExpect(status().isOk());
    }

    @Test(expected=AuthenticateException.class)
    public void testCustomerController_put_no_token() throws Exception {
        mockMvc.perform(get("/api/v1/customers/00000000-0000-0000-0000-000000000004")
                .contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    private MockHttpServletRequestBuilder createPostReqAuth() {
        return addAuthHeader(post("/api/v1/customers").contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    private MockHttpServletRequestBuilder addAuthHeader(final MockHttpServletRequestBuilder builder) {
        return builder.header(HttpHeaders.AUTHORIZATION, "Bearer token");
    }
}