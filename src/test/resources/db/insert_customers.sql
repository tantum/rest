INSERT INTO rest_db.customer (id, title, is_deleted, created_at)
    VALUES ('00000000-0000-0000-0000-000000000001', 'title1', false, 'now()');

INSERT INTO rest_db.customer (id, title, is_deleted, created_at)
    VALUES ('00000000-0000-0000-0000-000000000002', 'title2', true, 'now()');

INSERT INTO rest_db.customer (id, title, is_deleted, created_at)
    VALUES ('00000000-0000-0000-0000-000000000003', 'title3', false, 'now()');
    
INSERT INTO rest_db.customer (id, title, is_deleted, created_at)
    VALUES ('00000000-0000-0000-0000-000000000004', 'title4', false, 'now()');
    
    
INSERT INTO rest_db.product (id, customer_id, price, title, is_deleted, created_at)
    VALUES ('50000000-0000-0000-0000-000000000001', '00000000-0000-0000-0000-000000000001', '10000000.1', 'product1', false, 'now()');

INSERT INTO rest_db.product (id, customer_id, price, title, is_deleted, created_at)
    VALUES ('50000000-0000-0000-0000-000000000002', '00000000-0000-0000-0000-000000000001', '10000000.2','product2', true, 'now()');

INSERT INTO rest_db.product (id, customer_id, price, title, is_deleted, created_at)
    VALUES ('50000000-0000-0000-0000-000000000003', '00000000-0000-0000-0000-000000000003', '10000000.3','product3', false, 'now()');
    
INSERT INTO rest_db.product (id, customer_id, price, title, is_deleted, created_at)
    VALUES ('50000000-0000-0000-0000-000000000004', '00000000-0000-0000-0000-000000000003', '10000000.4','product4', false, 'now()');